const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
var nunjucksRender = require('gulp-nunjucks-render');
var reload = browserSync.reload;

gulp.task('sass', () => {
    return gulp.src([
            'app/sass/styles.scss',
            'node_modules/tail.datetime/css/tail.datetime-default-red.min.css'
        ])
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest('./app/css'))
        .pipe(browserSync.stream());
});

gulp.task('nunjucks', function() {
    return gulp.src('app/pages/**/*.+(html|njk)')
    .pipe(nunjucksRender({
        path:['app/templates'],
        watch:true,
    }))
    .pipe(gulp.dest('./app'))
});

gulp.task('js', () => {
    return gulp.src([           
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/wow.js/dist/wow.js',
        'node_modules/siema/dist/siema.min.js',
        'node_modules/tail.datetime/js/tail.datetime.min.js',
        'node_modules/tail.datetime/langs/tail.datetime-es_MX.js'
        
        
        ])
        .pipe(gulp.dest('./app/js'))
        .pipe(browserSync.stream());
});


gulp.task('js-watch', ['js'], reload);
gulp.task('css-watch', ['sass'], reload);
gulp.task('nunjucks-watch', ['nunjucks'], reload);

gulp.task('server', ['sass'], () => {
    browserSync.init({
        server: './app'
    });

    gulp.watch([
        './app/sass/*.scss'             
    ], ['css-watch']);

    gulp.watch([        
        './app/js/*.js'
    ], ['js-watch']);
    gulp.watch([
        'app/pages' + '/**/*.+(html|njk)',
        'app/templates' + '/**/*.+(html|njk)'
    ],['nunjucks']);

    gulp.watch('./app/*.html')
    .on('change', browserSync.reload);

});

gulp.task('default', ['js','nunjucks','server'])