var icon = document.getElementById('click');
var overlay = document.getElementById("overlay");

function openNav() {
    document.getElementById("menulinks").style.width = "250px";
    document.querySelector('body').style.overflow = "hidden"
    overlay.style.opacity = '.7'
    overlay.style.height = '100vh'
    icon.setAttribute('onclick', 'closeNav()');
    icon.innerHTML = "<img src='img/close.svg' alt=''>";

}

function closeNav() {
    document.getElementById("menulinks").style.width = "0";
    document.querySelector('body').style.overflow = "inherit"
    overlay.style.opacity = '0';
    overlay.style.height = '0'
    icon.setAttribute('onclick', 'openNav()');
    icon.innerHTML = "<img src='img/menu.svg' alt=''>";
}

// Nav Novedades

Siema.prototype.addPagination = function () {
    var container = document.querySelector('#siema-news')
    var botonera = document.createElement('div')
    botonera.setAttribute('id', 'botonera')
    for (let i = 0; i < this.innerElements.length; i++) {
        const btn = document.createElement('button');
        botonera.appendChild(btn)
        container.appendChild(botonera);
        var botones = botonera.childNodes;
        botones[0].classList.add('active')
        btn.addEventListener('click', () => {
            for (let b = 0; b < botones.length; b++) {
                var activo = botones[b];
                if (activo.classList.contains('active')) {
                    activo.classList.remove('active')
                }
            }
            this.goTo(i);
            let current = this.currentSlide.toString();
            btn.classList.add('active')
        })
    }
}



function slideHeader() {
    var siema = new Siema({
        selector: '#siema-banners',
        duration: 200,
        easing: 'ease-out',
        perPage: 1,
        startIndex: 0,
        draggable: true,
        multipleDrag: true,
        threshold: 20,
        loop: true,
        rtl: false
    });
    const prev = document.querySelector('#prev');
    const next = document.querySelector('#next');
    prev.addEventListener('click', () => siema.prev());
    next.addEventListener('click', () => siema.next());
}

function slideNews() {
    var novedades = new Siema({
        selector: '#siema-news',
        duration: 200,
        easing: 'ease-out',
        perPage: {
            768: 2,
            1024: 3
        },
        startIndex: 0,
        draggable: true,
        multipleDrag: true,
        threshold: 20,
        loop: false,
        rtl: false

    });

    showHomeSlider();
    window.addEventListener('resize', showHomeSlider);
    function showHomeSlider() {
        if (window.matchMedia('(max-width: 768px)').matches) {
            /* paginado */
            novedades.init();
            novedades.addPagination();
        } else {
            novedades.destroy(true);
        }
    }
}


function goBack() {
    window.history.back();
}